const FeatureFlags = require('./feature-flags');

describe('FeatureFlags Constructor', () => {
    test('It cannot be constructed with no fields', () => {
        try {
            new FeatureFlags();
        } catch (error) {
            expect(error).toBeInstanceOf(TypeError);
        }
    });

    test('It cannot be constructed with no dates', () => {
        try {
            new FeatureFlags('test-name');
        } catch (error) {
            expect(error).toBeInstanceOf(TypeError);
        }
    });

    test('It can be constructed without a start time', () => {
        const endTime = new Date('2018-12-31 00:00:00');
        const sut = new FeatureFlags('test-name', endTime);
    });

    test('It can be constructed without a start time and sets the default to now', () => {
        const endTime = new Date('2018-12-31 00:00:00');
        const sut = new FeatureFlags('test-name', endTime);
        const d = new Date();
        expect(sut.startTime).toBe(d.toISOString());
    });

    test('It assigns all values correctly when they are all provided', () => {
        const endTime = new Date('2018-12-31 00:00:00');
        const startTime = new Date('2018-12-23 00:00:00')
        const sut = new FeatureFlags('test-name', endTime, startTime);
        expect(sut.name).toBe('test-name');
        expect(sut.endTime).toBe(endTime.toISOString());
        expect(sut.startTime).toBe(startTime.toISOString());
    });
});
