class FeatureFlags {
    constructor(name, endTime, startTime) {
        if (undefined === name || undefined === endTime) {
            throw TypeError('name, endTime are required fields');
        }

        this.name = name;
        this.endTime = endTime.toISOString();
        this.startTime = (undefined === startTime) ? new Date().toISOString() : startTime.toISOString();
    }
}

module.exports = FeatureFlags;