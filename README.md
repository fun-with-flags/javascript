# Fun With Flags

## What?

This library is designed to handle time based feature flags, allowing you to fetch & retrieve feature flags from a Postgres database and check whether they are currently enabled

## Why?

There are 2 reasons I've started to write this library, the first is that recently I've become really interested in using Feature Flags for the separation of a deployment and a release.

The second reason is that I believe Feature Flags should have a lifespan, you don't want your code to be littered with `if` statements for testing whether a flag is enabled, and if there's a lifespan set on your Feature Flag then you'll be forced to make a decision on whether to keep a feature or not.
